
# Reference in English
This is an application for sending new messages from the database to Kafka.
## Requirements
- Python 3.6-...
- packages specified in `requirements.txt`
## Installation, launch, uninstallation
1. Make a copy of the repository: `https://gitlab.com/IdeenGroup/postgrestokafkatransfer.git`
2. Go to project folder - cd [path to the directory where the project is located]/postgrestokafkatransfer
3. Install missing packages - pip install -r requirements.txt
4. Go to the director src - cd src
4. Launch the application - python sending_new_data.py
## Working with the application
- You need to access postgre, you need to specify the data.
- Specify topic names in Kafka.
- Further, if the message is not yet on the topic, then this message from postgre will be sent to Kafka.
## Docker
You can run this application in docker
### Prerequirements
Clone this project
### Envirement variables
You can you use `.env` file as it or change itself with your needs
### Build
Build docker image
```
docker-compose build
```
### Run
Run docker containers
```
docker-compose up -d
```


# Reference in Russian
Это приложение для отправки новых сообщений из базы данных в Kafka.
## Требования
- Python 3.6-...
- пакеты, указанные в `requirements.txt`
## Установка, запуск, удаление
1. Cоздать копию репозитория: `https://gitlab.com/IdeenGroup/postgrestokafkatransfer.git`
2. Перейти в папку проекта - cd [путь к директории в которой лежит проект]/postgrestokafkatransfer
3. Установить недостающие пакеты - pip install -r requirements.txt
4. Перейти в диреторию src - cd src
4. Запустить приложение - python sending_new_data.py
## Работа с приложением
- Необходимо получить доступ к postgre, нужно указать данные.
- Указать названия топика в Kafka.
- Далее, если сообщение ещё не лежит на топике, то это сообщение из postgre будет отправлено в Kafka.
## Docker
Данные приложение может быть запушено в докере
### Prerequirements
Вам необходимо склонировать данный проект
### Envirement variables
Вы можете использовать `.env` файл из репозитория или кастомизировать его под себя
### Build
Соберите докер-образы
```
docker-compose build
```
### Run
Запустите необходимые контейнемы
```
docker-compose up -d
```

