import os

POSTGRES = {
    'HOST': os.getenv('DB_HOST', 'localhost'),
    'PORT': os.getenv('DB_PORT', '6543'),
    'NAME': os.getenv('DB_NAME', 'ideen'),
    'USER': os.getenv('DB_USER', 'sixhands'),
    'PASS': os.getenv('DB_PASS', 'sixhands'),
}

KAFKA = {
    'BOOTSTRAP_SERVER': os.getenv('KAFKA_BOOTSTRAP_SERVER', 'localhost:9092').split(','),
    'OUTPUT_TOPIC': os.getenv('KAFKA_OUTPUT_TOPIC', 'output')
}

GENERAL = {
    'BATCH_SIZE': int(os.getenv('BATCH_SIZE', '1000'))
}