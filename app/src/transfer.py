import time
from json import dumps
from kafka import KafkaProducer
from ulid import from_timestamp
from datetime import datetime

from setting import GENERAL, POSTGRES, KAFKA

import psycopg2
import logging
import os

logging.basicConfig(
    level=int(os.environ.get("DEBUG_LEVEL", logging.INFO)),
    format="[%(asctime)s][%(process)d][%(thread)d]:" + logging.BASIC_FORMAT,
    handlers=[
        logging.StreamHandler()
    ]
)

log = logging.getLogger(__name__)


def main():
    log.info('starting...')
    log.info(f"connecting to postgres {POSTGRES['HOST']}:{POSTGRES['PORT']}/{POSTGRES['NAME']} as {POSTGRES['USER']}")
    connection_instance = psycopg2.connect(
        host=POSTGRES['HOST'],
        port=POSTGRES['PORT'],
        database=POSTGRES['NAME'],
        user=POSTGRES['USER'],
        password=POSTGRES['PASS']
    )

    topic_name = 'patents-raw-0'
    log.info(f"connecting to kafka {KAFKA['BOOTSTRAP_SERVER']}")
    producer = KafkaProducer(
        bootstrap_servers=KAFKA['BOOTSTRAP_SERVER'],
        value_serializer=lambda x: dumps(x).encode('utf-8'),
        max_request_size=50_000_000
    )
    log.info(f"sending data to topic {topic_name}")

    cycle = True
    while cycle:
        log.info(f"loading batch of {GENERAL['BATCH_SIZE']} elements")
        query = f"""
            SELECT title, author_last_name, author_first_name, author_country, abstract, description, id FROM 
                (SELECT patent_id FROM unhandled_patents LIMIT {GENERAL['BATCH_SIZE']}) up
            LEFT JOIN 
                patents p
            ON p.id = up.patent_id
        """
        cursor = connection_instance.cursor()
        cursor.execute(query)
        rows = cursor.fetchall()
        cursor.close()

        ids = []
        log.info(f"sending data")
        for row in rows:
            try:
                ids.append(int(row[6]))
                content = {
                    'title': row[0],
                    'author_first_name': row[1],
                    'author_last_name': row[2],
                    'author_country': row[3],
                    'abstract': row[4],
                    'description': row[5],
                }
                ulid = str(from_timestamp(time.time()))
                content['ULID'] = ulid
                content['Chronology'] = [
                    {
                        'TransformerType': 'PostgresConnector',
                        'Date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'TransformerVersion': '0.2'
                    }
                ]
                future = producer.send(
                    topic=KAFKA['OUTPUT_TOPIC'],
                    key=str.encode(ulid),
                    value=content,
                )
                record_metadata = future.get(timeout=10)
                log.debug('--> The message has been sent to a topic: \
                        {}, partition: {}, offset: {}' \
                          .format(record_metadata.topic,
                                  record_metadata.partition,
                                  record_metadata.offset))
            except Exception as e:
                log.error('--> It seems an Error occurred: {}'.format(e))
        log.info(f"delete indexes from unhandled_patents")
        query = f"""
            DELETE FROM unhandled_patents WHERE patent_id in ({str(ids)[1:-1]})
        """
        cursor = connection_instance.cursor()
        cursor.execute(query)
        cursor.close()
        log.info(f"batch of {GENERAL['BATCH_SIZE']} was parsed")
    connection_instance.close()
